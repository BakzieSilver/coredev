<?php 
$libComponents = "lib/components";

?>

<!DOCTYPE html>
<html lang="en" ng-app>
<head>
  <meta charset="utf-8">
  <title>My HTML File</title>
  <link rel="stylesheet" href="<?php echo $libComponents; ?>bootstrap-less/less/bootstrap.css">
  <link rel="stylesheet" href="css/app.css">
  <script src="<?php echo $libComponents; ?>angular/angular.js"></script>
</head>

<body>
</body>
</html>