Meant to house CSS components, this folder is the bread and butter of the web apps styling. It contains a file per
component (such as a button, input field, etc.), each of which describe the rules that affect that component only.

These are intended to be re-usable and transportable from project to project, aside for obviously being re-usable
inside the current webapp.