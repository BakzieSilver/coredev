This folder is intended to house any files that need to be shared across either different apps
or by different views within this single app; these can be various, specialized libraries
that aren't part of any of the existing frameworks, for instance.

NOTE: This is also meant for stuff like bootstrap, jquery and whatever else common libraries
we pull through Bower.