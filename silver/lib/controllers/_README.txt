Meant to house all the different controllers and respective parent classes (or component objects, depending
on the kind of architecture you're going for) that are used as part of this web app to manipulate the model.

Obviously, making components which can be transferred from project to projet is most helpful.