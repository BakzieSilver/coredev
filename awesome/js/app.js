var app = angular.module('marionApp', [
	'ngRoute',
	'LoginCtrl',
	'DashCtrl',
	'FrontCtrl',
	'AuthSrvc',
	'CrudSrvc',
	'ui.tinymce'
]);

app.run(function($rootScope, $location, Login){
	$rootScope.$on('$routeChangeStart', function(event,next,current) {
		var whiteList	= ['/', '/admin/login', '/teenused', '/meist'];
		var loggedIn	= Login.checkLoginStatus();
		var routeSafe	= $.inArray($location.path(),whiteList) > -1;
		if(!loggedIn && !routeSafe) {
            $location.path('/');
            alert('You must be logged in to view this page!');
        }
	});
});

//Route handler
app.config(function($routeProvider, $locationProvider){
	$locationProvider.html5Mode(true);
	var adminPrefix = '/admin';
	$routeProvider
		.when('/', {
			templateUrl:'js/templates/front.html',
			controller:'FrontController'
		})
		.when('/teenused', {
			templateUrl:'js/templates/services.html',
			controller:'ServicesController'
		})
		.when('/meist', {
			templateUrl:'js/templates/about_us.html',
			controller:'AboutController'
		})
		.when(adminPrefix+'/login', {
			templateUrl:'js/templates/login.html',
			controller:'LoginController'
		})
		.when(adminPrefix+'/dashboard',{
			templateUrl:'js/templates/dashboard.html',
			controller:'DashController'
		})
		.when(adminPrefix+'/add', {
			templateUrl:'js/templates/add.html',
			controller:'PostController'
		}).
		when(adminPrefix+'/edit/:id',{
			templateUrl:'js/templates/edit.html',
			controller:'EditPostController'
		});
});

app.filter('addEllipsis', function(){
	return function(input,$scope){
		if(input) {
			return input + '...';
		}
	}
});