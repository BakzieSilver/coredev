<!DOCTYPE html>
<html lang="et">
    <head>
        <title>Inna Sulg Esinemiskunsti Kool</title>
        <base href="/">

        <!-- Styles -->
        <link href="//fonts.googleapis.com/css?family=Lato:100,300,700" rel="stylesheet" type="text/css">
        <link href="components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="css/main.css" rel="stylesheet" type="text/css">

        <!-- Scripts -->
        <script src="components/jquery/dist/jquery.js"></script>
        <script src="components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="components/angular/angular.js"></script>
        <script src="components/angular-route/angular-route.js"></script>
        <script src="components/tinymce-dist/tinymce.min.js"></script>
        <script src="components/angular-ui-tinymce/src/tinymce.js"></script>
        <script src="components/angular-sanitize/angular-sanitize.js"></script>

        <!-- App -->
        <script src="js/app.js"></script>

        <!-- Controllers -->
        <script src="js/controllers/loginController.js"></script>
        <script src="js/controllers/dashController.js"></script>
        <script src="js/controllers/frontController.js"></script>

        <!-- Services -->
        <script src="js/services/authService.js"></script>
        <script src="js/services/crudService.js"></script>

    </head>

    <body ng-app="marionApp">
        <?php 
            include 'navbar.php';
        ?>