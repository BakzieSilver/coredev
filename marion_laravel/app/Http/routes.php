<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(array('prefix'=>'/api'),function(){
	Route::post('login/auth','Auth\AuthController@Login');
	Route::get('login/destroy','Auth\AuthController@Logout');

	Route::resource('posts','PostController');
});

Route::group(array('before'=>'auth','prefix'=>'/api'),function(){
});

Route::get('/{angular}', function(){
	return view('index');
})->where('angular','.*');

/*Route::get('/', function () {
    return view('index');
});*/