<?php

namespace App\Http\Controllers;

use Request;
use App\Post;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $posts = Post::all();
        return $posts;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $new = new Post;
        $new->post_author = "Admin";
        $new->post_title = Request::get('post_title');
        $new->post_contents = Request::get('post_contents');
        $new->post_type = "News";
        $new->user_id = 0;
        $new->page_id = 0;

        if($new->save()){
            return array('status' => 'Saved.');
        }
        return array('status' => 'Not saved.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Post::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $post = Post::find($id);
        if($post) {
            $post->post_title = Request::get('post_title');
            $post->post_contents = Request::get('post_contents');
            $post->live = Request::get('live');

            if($post->save()){
                return array('status'=>'Updated!');
            } else {
                return array('status'=>'Could not update.');
            }
        }
        return array('status'=>'Could not find post.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $remove = Post::where('id',$id)->delete();
        if($remove){
            return array('status'=>'Post successfully deleted.');
        }
        return array('status'=>'Could not delete post '.$id);
    }
}
